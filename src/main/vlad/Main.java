package vlad;


import vlad.controller.FamilyController;
import vlad.dao.CollectionFamilyDao;
import vlad.humans.Family;
import vlad.humans.Human;
import vlad.humans.Man;
import vlad.humans.Woman;
import vlad.dao.FamilyDao;
import vlad.pets.Dog;
import vlad.pets.Pet;
import vlad.service.FamilyService;

import java.text.ParseException;
import java.util.*;

public class Main {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static FamilyService createFamily() {
        Human child1 = new Man("Vlad", "Lietun");
        child1.setBirthDate("16.03.1995");
        Human child2 = new Man("Nick", "Bubalo");
        Human woman = new Woman("Elena", "Lietun", "23.03.1972", 50, null);
        Human man = new Man("Dima", "Lietun", "23.09.1973", 50, null);

        Family family = new Family(woman, man);
        Set<Pet> pets = new HashSet<>();

        Dog tuzik = new Dog("Tuzik", 5, 20, null);
        Dog bobik = new Dog("Bobik", 10, 50, null);

        pets.add(tuzik);
        pets.add(bobik);
        family.addChild(child1);
        family.setPet(pets);

        Human woman1 = new Woman("Kate", "Bubalo");
        Human man1 = new Man("Bob", "Bubalo");
        Family family1 = new Family(woman1, man1);
        family1.addChild(child2);

        FamilyDao familyDao = new CollectionFamilyDao();
        familyDao.saveFamily(family);
        familyDao.saveFamily(family1);

        FamilyService familyService = new FamilyService(familyDao);
        return familyService;
    }

    public static void main(String[] args) {

        String command;
        FamilyController familyController = null;

        do {
            printCommand();

            System.out.print("\nPlease, enter number of command: ");
            switch (command = SCANNER.next()) {
                case "1":
                    System.out.println("-1. Fill with test data:\n");
                    familyController = new FamilyController(createFamily());
                    System.out.println(familyController.getAllFamilies());
                    break;

                case "2":
                    System.out.println("-2. Display all list of families:\n");
                    if (familyController != null) {
                        familyController.displayAllFamilies();
                    } else warningCreateTest();
                    break;

                case "3":
                    System.out.println("-3. Display a list of families where the number of people is more than the specified:");
                    System.out.print("\t-Enter your number: ");
                    int number = SCANNER.nextInt();
                    System.out.println();
                    if (familyController != null) {
                        System.out.println(familyController.getFamiliesBiggerThan(number));
                    } else warningCreateTest();
                    break;

                case "4":
                    System.out.println("-4. Display a list of families where the number of people is less than the specified:");
                    System.out.print("\t-Enter your number: ");
                    int number1 = SCANNER.nextInt();
                    System.out.println();
                    if (familyController != null) {
                        System.out.println(familyController.getFamiliesLessThan(number1));
                    } else warningCreateTest();
                    break;

                case "5":
                    System.out.println("-5. Count the number of families where the number of members is:");
                    System.out.print("\t-Enter your number: ");
                    int number2 = SCANNER.nextInt();
                    System.out.println();
                    if (familyController != null) {
                        System.out.println(familyController.countFamiliesWithMemberNumber(number2));
                    } else warningCreateTest();
                    break;


                case "6":
                    System.out.println("-6. Create new family:");
                    Human mother = createWoman();
                    Human father = createMan();
                    if (familyController != null) {
                        familyController.createNewFamily(
                                new Woman(mother.getName(), mother.getSurname(), mother.getBirthDate(), mother.getIq()),
                                new Man(father.getName(), father.getSurname(), father.getBirthDate(), father.getIq()));
                    }
                    System.out.println("Congratulation, your family added to main list of families!");
                    break;

                case "7":
                    System.out.println("-7. Delete family by family index in the general list:");
                    System.out.print("\t-Enter your number: ");
                    int index = SCANNER.nextInt();
                    if (familyController != null) {
                        familyController.deleteFamily(index);
                    }
                    System.out.println("Family by index " + index + " has been deleted.");
                    break;

                case "8":
                    System.out.println("-8. Edit family by family index in the general list: ");
                    System.out.println("\t-1. Add child to family\n" + "\t-2. Adopt child\n" + "\t-3. Return to the main menu");
                    System.out.print("Please, enter number of command: ");
                    int number3 = SCANNER.nextInt();

                    if (number3 == 1) {
                        System.out.print("\t-Enter family number(ID): ");
                        int familyId = SCANNER.nextInt();
                        System.out.print("\t-Enter boy name: ");
                        String boyName = SCANNER.next();
                        System.out.print("\t-Enter girl name: ");
                        String girlName = SCANNER.next();

                        if (familyController != null) {
                            Family familyById = familyController.getFamilyById(familyId);
                            familyController.bornChild(familyById, boyName, girlName);
                        }
                        System.out.println("Child added to family!");
                    }

                    if (number3 == 2) {
                        System.out.print("\t-Enter family number(ID): ");
                        int familyId = SCANNER.nextInt();
                        System.out.print("\t-Enter all information about child. If you want to adopt boy press 1 or girl pres 2: ");
                        int chosenNumber = SCANNER.nextInt();

                        assert familyController != null;
                        Family familyById = familyController.getFamilyById(familyId);

                        if (chosenNumber == 1) {
                            familyController.adoptChild(familyById, createMan());
                        } else if (chosenNumber == 2) {
                            familyController.adoptChild(familyById, createWoman());
                        } else {
                            System.out.println("Enter correct number!");
                        }
                        System.out.println("Congratulation, you adopted child to your family!");
                    }

                    break;

                case "9":
                    System.out.println("-9. Remove all children over age: ");
                    System.out.print("\t-Enter your age: ");
                    int age = SCANNER.nextInt();
                    try {
                        assert familyController != null;
                        familyController.deleteAllChildrenOlderThen(age);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
            }

        } while (!command.equalsIgnoreCase("exit"));

    }

    public static void printCommand() {
        System.out.println(
                "\n-1. Fill with test data.\n" +
                        "-2. Display all list of families.\n" +
                        "-3. Display a list of families where the number of people is more than the specified.\n" +
                        "-4. Display a list of families where the number of people is less than the specified.\n" +
                        "-5. Count the number of families where the number of members is.\n" +
                        "-6. Create new family.\n" +
                        "-7. Delete family by family index in the general list.\n" +
                        "-8. Edit family by family index in the general list.\n" +
                        "-9. Remove all children over age."
        );
    }

    public static void warningCreateTest() {
        System.out.println("Create test data family. Enter number 1");
    }

    public static Human createWoman() {
        System.out.print("\t-What is woman name? ");
        String womanName = SCANNER.next();
        System.out.print("\t-What is woman surname? ");
        String womanSurname = SCANNER.next();
        System.out.print("\t-What is woman year of birth? ");
        int womanYearBirth = SCANNER.nextInt();
        System.out.print("\t-What is woman month of birth? ");
        int womanMonthBirth = SCANNER.nextInt();
        System.out.print("\t-What is woman day of birth? ");
        int womanDayBirth = SCANNER.nextInt();
        System.out.print("\t-What is woman iq? ");
        int womanIq = SCANNER.nextInt();

        String resultBirthDate = womanDayBirth + "." + womanMonthBirth + "." + womanYearBirth;

        Human woman = new Woman(womanName, womanSurname, resultBirthDate, womanIq);
        return woman;
    }

    public static Human createMan() {
        System.out.print("\t-What is man name? ");
        String manName = SCANNER.next();
        System.out.print("\t-What is man surname? ");
        String manSurname = SCANNER.next();
        System.out.print("\t-What is man year of birth? ");
        int manYearBirth = SCANNER.nextInt();
        System.out.print("\t-What is man month of birth? ");
        int manMonthBirth = SCANNER.nextInt();
        System.out.print("\t-What is man day of birth? ");
        int manDayBirth = SCANNER.nextInt();
        System.out.print("\t-What is man iq? ");
        int manIq = SCANNER.nextInt();

        String resultBirthDate = manDayBirth + "." + manMonthBirth + "." + manYearBirth;

        Human woman = new Woman(manName, manSurname, resultBirthDate, manIq);
        return woman;
    }
}



